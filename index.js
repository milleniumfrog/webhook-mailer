const express = require('express');
const process = require('process');
const logupts = require('logupts');
const mailer = require('nodemailer');
const Remarkable = require('remarkable').Remarkable;

const main = async () => {
    const logger = new logupts.LogUpTs();
    const app = express();
    // get mail account
    const account = await mailer.createTestAccount();
    /** run webservice on port @type {number} */
    const PORT = Number(process.env.PORT) || 8081;
    /** send mails over this smtpserver */
    const SMTPHOST = process.env.SMTP || "smtp.ethereal.email";
    /** smtp server port */
    const SMTPPORT = process.env.SMTPPORT || 587;
    /** uses smtpsecure the secure protocol */
    const SMTPSECURE = Boolean(process.env.SMTPSECURE) || false;
    /** mail user */
    const SMTPUSER = process.env.SMTPUSER || account.user;
    /** mail password */
    const SMTPPW = process.env.SMTPPW || account.pass;
    const AUTH = process.env.AUTH || false;
    const transporter = mailer.createTransport({
        host: SMTPHOST,
        port: SMTPPORT,
        secure: SMTPSECURE,
        auth: AUTH ? {
            user: SMTPUSER,
            pass: SMTPPW
        } : undefined,
    });

    app.use(express.json())
    /**
     * endpoint to recive mattermost webhooklike requests and 
     */
    app.post("/hooks/mail", async (req, resp) => {
        try {
            // check if text, mail exist:
            if (!req.body || !req.body.text || !(typeof req.body.text === 'string')) {
                resp.status(400).send('expects property text : string')
                return;
            }
            else if (!req.body || !req.body.mail || !(typeof req.body.mail === 'string')) {
                resp.status(400).send('expects property mail : string')
                return;
            }
            // Remarkable renders md => html
            const rem = new Remarkable();
            // send mail
            const mailInfo = await transporter.sendMail({
                from: `"webhook mailer <${SMTPUSER}>"`,
                to: req.body.mail,
                subject: req.body.subject || process.env.SUBJECT || "ALERT",
                html: rem.render(req.body.text)
            });
            // generate output
            if (!process.env.SMTP)
                logger.info(`Preview URL: ${mailer.getTestMessageUrl(mailInfo)}`);
            else
                logger.info(`send mail from ${process.env.SMTPUSER || account.user}> to ${req.body.mail}`)
            resp.send("ok")
        }
        catch (err) {
            // print error with stack
            logger.error(err);
            // just send errormessage as response
            resp.status(500).send(err.message);
        }
    })

    const server = app.listen(PORT, () => {
        logger.info(`started server on ${PORT}`);
    })

    process.on('SIGINT', () => {
        console.log('stop the server')
        server.close()
        process.exit()
    })
}

main();