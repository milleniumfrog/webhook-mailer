# WEBHOOK MAILER
send mails via a webhook like rest api.

## SETUP
```YAML
version: '3.7'

services:
  webhookmailer:
    image: registry.gitlab.com/milleniumfrog/webhook-mailer:latest
    # only uncomment for testing
    # ports:
    #   - 8081:8081
    environment:
      PORT: 8081 # defaultvalue
      SMTP: "[your smtpserver]"
      SMTPPORT: 465
      SMTPSECURE: "true"
      SMTPUSER: "[your mail address]"
      SMTPPW: "[your password]"
      SUBJECT: "ALARM"
```

## USAGE:
curl:
```bash
$ curl --request POST \
  --url http://localhost:8081/hooks/mail \
  --header 'content-type: application/json' \
  --data '{
	    "mail":"your mail address",
	    "text": "# ALERT LEVEL 1\n this is a test \n\n begins: now"
    }'
```
nodejs:
```javascript
var request = require("request");

var options = {
  method: 'POST',
  url: 'http://localhost:8081/hooks/mail',
  headers: {'content-type': 'application/json'},
  body: {
    mail: 'your mail address',
    text: '# ALERT LEVEL 1\n this is a test \n\n begins: now'
  },
  json: true
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```

## CAUTION
This service is created to operate in a private network, do not expose the webhook endpoint to the public.