FROM node:10

COPY package.json package.json
COPY package-lock.json package-lock.json
COPY index.js index.js

RUN npm ci

CMD ["node", "index.js"]